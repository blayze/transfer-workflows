import json
import boto3
import os 

table_name = '' # need dynamodb table
topic_arn = '' # need sns topic

s3 = boto3.resource('s3')
transfer = boto3.client('transfer')
sns = boto3.client('sns')
db_table = boto3.resource('dynamodb').Table(table_name)

def save_to_dynamodb(uid, name, co):
    return db_table.put_item(
        Item={
            'uid': int(uid),
            'Name': name,
            'Company': co
        })

def send_step_state(workflowId, executionId, token, status):
    return transfer.send_workflow_step_state(
        WorkflowId=workflowId,
        ExecutionId=executionId,
        Token=token,
        Status=status
    )

def send_notification(message):
    return sns.publish(
        TopicArn=topic_arn,
        Message=message,
    )

def lambda_handler(event, context):

    bucket = event['fileLocation']['bucket']
    key = event['fileLocation']['key']
    workflowId = event['serviceMetadata']['executionDetails']['workflowId']
    executionId = event['serviceMetadata']['executionDetails']['executionId']
    token = event['token']
    status = 'FAILURE'
    
    # Temporary file to store csv data
    tmp_csv = '/tmp/' + key.rsplit('/', 1)[-1]
    
    s3.meta.client.download_file(
        bucket,
        key,
        tmp_csv)

    # Check that the file is a csv
    if os.path.isfile(tmp_csv):
        FileExtension = os.path.splitext(tmp_csv)[1]
        if FileExtension.lower() == ".csv":
            # Save data to DynamoDB
            with open(tmp_csv, 'r') as f:
                next(f) # skip header
                for line in f:
                    uid, name, co = line.rstrip().split(',')
                    ddb_response = save_to_dynamodb(uid, name, co)
                    print(ddb_response)
            status = 'SUCCESS'
    
    notification = workflowId + " " + executionId + " " + status
    send_notification(notification)
    
    response = send_step_state(workflowId,executionId,token,status)
    
    return {
        'statusCode': 200,
        'body': json.dumps(response)
    }

