import json
import boto3

topic_arn = '' # need sns topic

sns = boto3.client('sns')
transfer = boto3.client('transfer')

def send_notification(message):
    return sns.publish(
        TopicArn=topic_arn,
        Message=message,
    )

def send_step_state(workflowId, executionId, token, status):
    return transfer.send_workflow_step_state(
        WorkflowId=workflowId,
        ExecutionId=executionId,
        Token=token,
        Status=status
    )

def lambda_handler(event, context):
    
    print(event)
    
    bucket = event['fileLocation']['bucket']
    key = event['fileLocation']['key']
    username = event['serviceMetadata']['transferDetails']['userName']
    workflowId = event['serviceMetadata']['executionDetails']['workflowId']
    executionId = event['serviceMetadata']['executionDetails']['executionId']
    token = event['token']

    message = "Workflow failed for user " + username + "'s file " + bucket + "/" + key + ". The workflow " + workflowId + " execution id is " + executionId 
    
    print(message)

    sns_response = send_notification(message)
    
    print(sns_response)
    
    status = 'SUCCESS'
    
    print(status)
    
    response = send_step_state(workflowId, executionId, token, status)
    
    print(response)
    
    return {
        'statusCode': 200,
        'body': json.dumps(response)
    }
