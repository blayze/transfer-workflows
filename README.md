# AWS Transfer Family managed workflows demo materials

## Purpose
The purpose of this repository is to provide materials to demo AWS Transfer Family managed workflows. Note that I have not tested the steps below thoroughly and this may need some updates. This is following a demo video I did via the AWS management console.

## Instructions 

### Assumptions
- You have access to perform all the actions below in an AWS account.
- You already have an AWS Transfer Family server, user, and logging role set up.

### Demo 1 - Data Archival - Copy and Tag
1. Create two S3 buckets. For this example, the destination bucket should have a lifecycle policy to archive to Glacier based on the key Archive and value yes, and bucket versioning enabled.
2. Update your `demo1.json` to include your destination bucket name and key path. Don't forget the forward slash on the key, or it will update the object with that key name every time.
3. Create your IAM role with a transfer.amazonaws.com trust policy, and IAM policy for S3 copy and tag permissions.
4. Create workflow: `aws transfer create-workflow --steps file://demo1-workflow-steps.json --description "data archival"`
5. Get the workflow ID as the output of the previous command, and put it into `demo1-workflow-details.json`. Add your execution role ARN.
6. Update your server: `aws transfer update-server --server-id s-123456789012 --workflow-details file://demo1-workflow-details.json`
7. Connect to your server and upload `demo1.csv`. To familiarize yourself, observe the workflow execution in CloudWatch Logs, and check your S3 buckets. 

### Demo 2 - Data Extraction - Custom and Delete
1. Create a DynamoDB Table based on `demo2.csv`. Make sure uid is an int. 
2. Create an SNS topic. Create a subscription to the SNS topic, such as an email subscriber.
3. Update your Lambda function variables for the DynamoDB table name and SNS topic ARN. Upload the function. If you are following this around the time of the feature relase, you need to add a Lambda layer for the latest boto3 client.
4. Create your IAM role with a transfer.amazonaws.com trust policy, and IAM policy for Lambda invocation and S3 delete permissions.
5. Update `demo2-workflow-steps.json` and `demo2-workflow-execution-steps.json` with your transferExtract Lambda function ARN. Update `demo2-workflow-execution-steps.json` with your exceptionHandler Lambda function ARN.
6. Create workflow: `aws transfer create-workflow --steps file://demo2-workflow-steps.json --on-exception-steps file://demo2-workflow-execution-steps.json --description "data extraction"`
7. Get the workflow ID as the output of the previous command, and put it into `demo2-workflow-details.json`. Add your execution role ARN.
8. Update your server: `aws transfer update-server --server-id s-123456789012 --workflow-details file://demo2-workflow-details.json`
9. Connect to your server and upload `demo2.csv`. To familiarize yourself, observe the workflow execution in CloudWatch Logs, and check your DynamoDB table, SNS subscriber, and Lambda function execution logs.
